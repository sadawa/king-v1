import React from 'react'
import {Link} from "react-router-dom";
import styled from "styled-components";

const Navbar = styled.nav`
height: 68px;
background: transparent;
padding: 0rem calc((100wv - 130wv)/2);
display: flex; 
justify-content: space-between;
align-items: center;


`

const Logo = styled(Link)`
color: white;
padding-left: 1rem;
text-decoration: none;
font-size: 1.5rem;
font-weight: bold;
font-style: italic;
`

const NavItems = styled.div`

`

const NavbarLink = styled(Link)`
color: white;
text-decoration: none;
padding: 1rem;

`

const Header = () => {
    return (
        <Navbar>
        <Logo to= "/">King</Logo>
        <NavItems>
            <NavbarLink to="/">Sukuna</NavbarLink>
            <NavbarLink to="/gojo">Gojo</NavbarLink>
            <NavbarLink to="/luffy">Luffy</NavbarLink>
        </NavItems>
        </Navbar>
    )
}

export default Header
