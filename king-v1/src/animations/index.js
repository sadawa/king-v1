/** dans ce fichier on utilie pour cree les animation est de importe dans les pages */

export const animeOne = {
    in: {
        opacity:1
    },
    out:{
        opacity:0
    }
}
export const animeTwo = {
    in: {
        opacity:1,
        y:0,
        scale: 1
    },
    out:{
        opacity:0,
        y: "-100vh",
        scale:0.4
    }
}
export const animeThree = {
    in: {
        opacity:1,
        x:-400,
       
    },
    out:{
        opacity:0,
        x: 400,
        
    },
    end: {
        x:0,
        opacity: 1
    }
}


export const transition = {
    duration: 0.4
}