import './App.css';
import { Switch, Route, useLocation} from "react-router-dom"
import Home from "./pages/Home"
import Gojo from './pages/Gojo';
import Luffy from './pages/Luffy';
import {AnimatePresence} from "framer-motion"
import GlobalStyle from './globalStyle';
import styled from "styled-components"

const Section = styled.section`
overflow-x: hidden;
`


function App() {
  let location = useLocation();
  return (
    <Section>
      <GlobalStyle/>
      <AnimatePresence exitBeforeEnter>
       <Switch location={location} key={location.pathname}> 
        <Route path="/" exact component={Home}/>
        <Route path="/gojo" component={Gojo}/>
        <Route path="/luffy" component={Luffy}/>
      </Switch>
      </AnimatePresence>
    </Section>
  );
}

export default App;
