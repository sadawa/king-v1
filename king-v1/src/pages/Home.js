import React from 'react'
import Header from '../components/Header'
import Hero from '../components/Hero'
import King from '../images/sukuna.jpg'
import {motion} from "framer-motion"
import { animeOne, transition } from '../animations'
const Home = () => {
    return (
          // motion qui permet faire les animations des pages 
        <motion.div
        initial="out" animate="in"exit="out"
        variants={animeOne} transition={transition}
        >
            <Header/>
            <Hero image={King} title="Sukuna"
            desc="Le roi des fleaux"/>
        </motion.div>
    )
}

export default Home
