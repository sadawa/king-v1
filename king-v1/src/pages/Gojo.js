import React from 'react'
import Header from '../components/Header'
import Hero from '../components/Hero'
import King from '../images/gojo.jpg'
import {motion} from 'framer-motion'
import { animeTwo} from "../animations/index"

const Gojo = () => {
    return (
        // motion qui permet faire les animations des pages 
        <motion.div
        initial="out"
        animate="in"
        exit="out"
        variants={animeTwo}
        >
            <Header/>
            <Hero image={King}
            title="Gojo"
            desc="Le puissant exorciste "/>
         
        </motion.div>
    )
}

export default Gojo;
