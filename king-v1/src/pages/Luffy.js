import { motion } from 'framer-motion'
import React from 'react'
import Header from '../components/Header'
import Hero from '../components/Hero'
import King from '../images/Luffy.png'
import { animeThree} from "../animations/index"

const Luffy = () => {
    return (
        <motion.div
        initial="out"
        animate="end"
        exit="out"
        variants={animeThree}
        >
            <Header/>
            <Hero image={King}
            title="Luffy"
            desc="Le futur roi des pirates"/>
           
        </motion.div>
    )
}

export default Luffy;
